import GuessGameComponent from './GuessGameComponent';

import * as React from 'react';
import {render, screen, fireEvent} from '@testing-library/react';

test('GuessGameComponent 有提供輸入框', () => {
  render(<GuessGameComponent />);
  const guessField = screen.getByRole('textbox');
  expect(guessField).toBeTruthy();
});

test('GuessGameComponent 有提供送出按鈕', () => {
  render(<GuessGameComponent />);
  const submitBtn = screen.getByRole('button');
  expect(submitBtn).toBeTruthy();
});

test('GuessGameComponent 有提供顯示結果的 heading', () => {
  render(<GuessGameComponent />);
  const resultHeading = screen.getByRole('heading');
  expect(resultHeading).toBeTruthy();
  expect(resultHeading).toHaveTextContent('快猜吧');
});

test('使用者送出猜測值後, 結果欄的內容會隨著改變', () => {
  render(<GuessGameComponent />);
  const guessed = '1357';
  const expectedResult = '1A1B';
  const guessField = screen.getByRole('textbox');
  const submitBtn = screen.getByRole('button');
  const resultHeading = screen.getByRole('heading');

  fireEvent.change(guessField, {target: {value: guessed}});
  fireEvent.click(submitBtn);

  expect(resultHeading).toHaveTextContent(expectedResult);
});
